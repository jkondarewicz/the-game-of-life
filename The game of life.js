const UPS = 20;
var gameStopped = true;
const xElements = 30;
const yElements = 30;

function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

var canvas = document.getElementById("life");
var ctx = canvas.getContext("2d");
var mouseMoveSubjects = [];
var mouseClickSubjects = [];

canvas.addEventListener('mousemove', function(evt) {
  var mousePos = getMousePos(canvas, evt);
  if(gameStopped) {
    for(var i = 0; i < mouseMoveSubjects.length; i++) {
      mouseMoveSubjects[i].updatePosition(parseInt(mousePos.x), parseInt(mousePos.y));
    }
  }
}, false);

canvas.addEventListener('click', function(evt) {
	var mousePos = getMousePos(canvas, evt);  
  if(gameStopped) {
    for(var i = 0; i < mouseClickSubjects.length; i++) {
      mouseClickSubjects[i].clicked(parseInt(mousePos.x), parseInt(mousePos.y));
    }
  }
}, false);

window.addEventListener( "keydown", function(key) {
  if(key.code === 'Space') {
    gameStopped = !gameStopped;
    document.getElementById("gameStopped").innerHTML = gameStopped ? "STOPPED" : "RUNNING SIMULATION";
  }
  if(key.code === 'ControlLeft') {
  	update();
    render();
  }
}, true);

const width = canvas.getAttribute('width');
const height = canvas.getAttribute('height');

const xElementSize = width / xElements;
const yElementSize = height / yElements;

function convertXIndexToWorldX(xIndex) {
	return xIndex * xElementSize;
}

function convertWorldXToXIndex(worldX) {
	return parseInt(worldX / xElementSize);
}

function convertYIndexToWorldY(yIndex) {
	return yIndex * yElementSize;
}

function convertWorldYToYIndex(worldY) {
	return parseInt(worldY / yElementSize);
}

function Quad(xIndex, yIndex, selected) {
	const that = this;
  this.xIndex = xIndex;
 	this.yIndex = yIndex;
  this.hovered = false;
  this.selected = selected;
	mouseMoveSubjects.push({
  	updatePosition: function(x, y) {
    	if(that.intersects(x, y)) {
        //console.log("Current on: (", that.xIndex, ", ", that.yIndex, ") quad" );
        that.hovered = true;
      } else {
      	that.hovered = false;
      }
    }
  })
  mouseClickSubjects.push({
  	clicked: function(x, y) {
    	if(that.intersects(x, y)) {
        //console.log("Clicked on: (", that.xIndex, ", ", that.yIndex, ") quad" );
        that.selected = !that.selected;
      }
    }
    	
  })
  
  this.intersects = function(x, y) {
  	const worldPos = that.convertToWorldPos();
    return x > worldPos.xLeft && x <= worldPos.xRight &&
          y > worldPos.yUp && y <= worldPos.yDown;
  }
  
  this.convertToWorldPos = function() {
  	const xLeft = parseInt(convertXIndexToWorldX(this.xIndex));
    const xRight = parseInt(convertXIndexToWorldX(this.xIndex + 1));
    const yUp = parseInt(convertYIndexToWorldY(this.yIndex));
    const yDown = parseInt(convertYIndexToWorldY(this.yIndex + 1));
    return {
    	xLeft, xRight, yUp, yDown
    };
  }

	this.render = function() {
  	const worldPos = this.convertToWorldPos();
    const x = worldPos.xLeft;
    const y = worldPos.yUp;
    const width = worldPos.xRight - worldPos.xLeft;
    const height = worldPos.yDown - worldPos.yUp;
    
    ctx.fillStyle = this.selected ? "#f00" : (this.hovered && gameStopped) ? "#f0f" : "#f8f";
    ctx.fillRect(x, y, width, height);
    
    ctx.strokeStyle = "#fff";
    ctx.lineWidth = 2;
    ctx.strokeRect(x, y, width, height);
    
  }
  
}


function Cell(xIndex, yIndex, alive) {
	
  this.xIndex = xIndex;
  this.yIndex = yIndex;
  this.alive = alive;
  this.nextAlive = false;
  
  this.setAlivnessInNextTurn = function(cells) {
  	var previousXIndex = this.xIndex - 1;
    var nextXIndex = this.xIndex + 1;
    var previousYIndex = this.yIndex - 1;
    var nextYIndex = this.yIndex + 1;
    var aliveNeighbors = this.alive ? -1 : 0;
    for(var x = previousXIndex; x <= nextXIndex; x++) {
    	for(var y = previousYIndex; y <= nextYIndex; y++) {
      	
        try {
        	if(cells[x][y].alive) {
            ++aliveNeighbors;
          }
        } catch(e) {
        	//index out of bounds exception
        }
      	
      }
    }
    //console.log("For cell(", this.xIndex, ", ", this.yIndex, ") neighbours is: ", aliveNeighbors);
    if(!this.alive && aliveNeighbors === 3) this.nextAlive = true;
    if(this.alive && aliveNeighbors >= 2 && aliveNeighbors <= 3) this.nextAlive = true;
  }
  
}


//Main game loop
var quads = [];
function initialize() {
  for(var i = 0; i < xElements; i++) {
    for(var j = 0; j < yElements; j++) {
      quads.push(new Quad(i, j, false));
    }
  }
}

function quadToCellConverter(quad) {
	return new Cell(quad.xIndex, quad.yIndex, quad.selected);
}

function cellToQuadConverter(cell) {
	return new Quad(cell.xIndex, cell.yIndex, cell.nextAlive);
}

function update() {
	/* console.log(deltaTime) */
  
  	var cells = [];
    for(var i = 0; i < quads.length; i++) {
    	const quad = quads[i];
      if(cells[quad.xIndex] === undefined) cells[quad.xIndex] = [];
      cells[quad.xIndex][quad.yIndex] = quadToCellConverter(quad);
    }
    quads = [];
    mouseMoveSubjects = [];
	mouseClickSubjects = [];
    for(var i = 0; i < cells.length; i++) {
    	for(var j = 0; j < cells[i].length; j++) {
      	cells[i][j].setAlivnessInNextTurn(cells);
        quads.push(cellToQuadConverter(cells[i][j]));
      }
    }
  
} 

function render() {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	for(var i = 0; i < quads.length; i++) {
    const quad = quads[i]
    quad.render();
  }
}

initialize();
function loop(timestamp) {
  var progress = timestamp - lastUpdate

	if(!gameStopped) {
  	if(progress > 1000 / UPS) {
    	//console.log(progress);
    	update()
      lastUpdate = timestamp;
    } 
  	
  }
  render()
  window.requestAnimationFrame(loop)
}
var lastUpdate = 0
window.requestAnimationFrame(loop)


